import mip

mip.install("pkg_resources")
mip.install("github:miguelgrinberg/microdot/src/microdot/microdot.py", target="/lib/microdot")
mip.install("github:miguelgrinberg/microdot/src/microdot/utemplate.py", target="/lib/utemplate")
mip.install("github:pfalcon/utemplate/utemplate/source.py", target="/lib/utemplate")
mip.install("github:pfalcon/utemplate/utemplate/compiled.py", target="/lib/utemplate")
mip.install("github:pfalcon/utemplate/utemplate/recompile.py", target="/lib/utemplate")
mip.install("jpegio")
mip.install("displayio")
