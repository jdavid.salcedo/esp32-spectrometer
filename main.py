from microdot.microdot import Microdot, Response, send_file
from utemplate.utemplate import Template
import camera
import ujson
from machine import Timer
from ulab import numpy as np
import ujpeg

# Setup camera
def initialize_camera():
    try:
        camera.init(0, format=camera.JPEG, fb_location=camera.PSRAM)
        camera.flip(0)
        camera.mirror(0)
        camera.saturation(1)
        camera.contrast(1)
        camera.brightness(1)
        camera.quality(10)
        print("Camera initialized successfully.")
    except Exception as e:
        print(f"Error initializing camera: {e}")

initialize_camera()

app = Microdot()
Response.default_content_type = 'text/html'

# Global variables
latest_image = None
image_width = 0
image_height = 0

# Function to decode JPEG header and get image dimensions
def get_image_dimensions(jpeg_data):
    # JPEG file format uses big-endian byte order
    def read_be_16(data, offset):
        return (data[offset] << 8) + data[offset + 1]

    offset = 2  # Skip the initial 0xFFD8 (SOI) marker
    while offset < len(jpeg_data):
        marker, length = read_be_16(jpeg_data, offset), read_be_16(jpeg_data, offset + 2)
        if marker == 0xFFC0:  # Start of frame marker (baseline DCT)
            height = read_be_16(jpeg_data, offset + 5)
            width = read_be_16(jpeg_data, offset + 7)
            return width, height
        offset += 2 + length
    return 0, 0

# Timer to periodically capture an image
def capture_periodically(timer):
    global latest_image, image_width, image_height
    try:
        latest_image = camera.capture()
        image_width, image_height = get_image_dimensions(latest_image)
        print(f"Image captured successfully. Width: {image_width}, Height: {image_height}")
    except Exception as e:
        print(f"Error capturing image: {e}")

# Set up a periodic timer to capture an image every 0.5 seconds
timer = Timer(0)
timer.init(period=500, mode=Timer.PERIODIC, callback=capture_periodically)

@app.route('/')
async def index(request):
    try:
        return Template('index.html').render()
    except Exception as e:
        print(f"Error rendering index: {e}")
        return 'Error rendering index', 500

@app.route('/capture')
async def capture_image(request):
    global latest_image
    try:
        if latest_image is None:
            return 'No image captured yet', 500
        headers = {'Content-Type': 'image/jpeg'}
        return Response(body=latest_image, headers=headers)
    except Exception as e:
        print(f"Error capturing image: {e}")
        return 'Error capturing image', 500

@app.route('/intensity/<int:y_position>', methods=['POST'])
async def intensity(request, y_position):
    global latest_image, image_width, image_height

    try:
        print(f"Y position: {y_position}")

        if latest_image is None or y_position < 0 or y_position >= image_height:
            return 'Invalid image or position', 500

        r, g, b = ujpeg.decode(latest_image, image_width, image_height)

        # Convert the RGB arrays to a 2D array representing the row at y_position
        r_row = r[y_position,:]
        g_row = g[y_position,:]
        b_row = b[y_position,:]

        # Calculate intensity as the mean of RGB values
        intensity_values = (r_row + g_row + b_row) / 3

        return Response(body=ujson.dumps(intensity_values.tolist()), headers={'Content-Type': 'application/json'})

    except Exception as e:
        print(f"Error computing intensity: {e}")
        return 'Error computing intensity', 500

@app.route('/shutdown')
async def shutdown(request):
    try:
        request.app.shutdown()
        return 'The server is shutting down...'
    except Exception as e:
        print(f"Error shutting down the server: {e}")
        return 'Error shutting down the server', 500

@app.route('/static/<path:path>')
def static(request, path):
    try:
        if '..' in path:
            # directory traversal is not allowed
            return 'Not found', 404
        return send_file('static/' + path)
    except Exception as e:
        print(f"Error serving static file {path}: {e}")
        return 'Error serving static file', 500

@app.route('/favicon.ico')
def favicon(request):
    return Response(status_code=204)  # No Content

try:
    app.run(host='192.168.58.107', port=8080, debug=True)
except Exception as e:
    print(f"Error starting the server: {e}")
