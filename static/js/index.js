let currentYPosition = 0;

async function fetchImage() {
    try {
        const response = await fetch('/capture');
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const blob = await response.blob();
        const url = URL.createObjectURL(blob);
        document.getElementById('image').src = url;
        return blob;
    } catch (error) {
        console.error('Error fetching image:', error);
    }
}

function getCrossSectionYPosition() {
    const crosshair = document.getElementById('crosshair');
    const image = document.getElementById('image');
    const rect = image.getBoundingClientRect();
    const crosshairRect = crosshair.getBoundingClientRect();
    const y_position = Math.round((rect.bottom - crosshairRect.bottom) * image.naturalHeight / rect.height);
    console.log('Cross section y position:', y_position);
    return y_position;
}

async function fetchIntensityValues(y_position) {
    try {
        const response = await fetch(`/intensity/${y_position}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            // body: JSON.stringify({ y_position: y_position })
        });
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const intensityValues = await response.json();
        return intensityValues;
    } catch (error) {
        console.error('Error fetching intensity values:', error);
    }
}

async function drawIntensityGraph(intensityValues) {
    const trace = {
        x: Array.from(Array(intensityValues.length).keys()),
        y: intensityValues,
        mode: 'lines',
        type: 'scatter'
    };

    const data = [trace];

    const layout = {
        title: 'Intensity Graph',
        xaxis: { title: 'Pixel Position' },
        yaxis: { title: 'Intensity' }
    };

    Plotly.newPlot('intensityGraph', data, layout);
}

async function update() {
    const imageBlob = await fetchImage();
    if (imageBlob) {
        // const y_position = getCrossSectionYPosition();
        const y_position = currentYPosition === 0 ? currentYPosition : getCrossSectionYPosition();
        const intensityValues = await fetchIntensityValues(y_position);
        if (intensityValues) {
            await drawIntensityGraph(intensityValues);
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const crosshair = document.getElementById('crosshair');
    const image = document.getElementById('image');

    let isDragging = false;

    const onMouseMove = (event) => {
        if (isDragging) {
            const rect = image.getBoundingClientRect();
            let newY = event.clientY - rect.top;
            if (newY < 0) newY = 0;
            if (newY > rect.height) newY = rect.height;
            crosshair.style.top = `${newY}px`;
            currentYPosition = Math.round(newY * (image.naturalHeight / rect.height));
        }
    };

    const onMouseUp = () => {
        if (isDragging) {
            isDragging = false;
            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup', onMouseUp);
            fetchIntensityValues(currentYPosition).then(intensityValues => {
                if (intensityValues) {
                    drawIntensityGraph(intensityValues);
                }
            });
        }
    };

    crosshair.addEventListener('mousedown', (event) => {
        isDragging = true;
        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseUp);
    });

    update();
    setInterval(update, 5000);
});
